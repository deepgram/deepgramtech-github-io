# DeepGram 

With this project we want to develop a Telegram bot to allow you running Machine Learning and Deep Learning experiments directly on your phone. 

The only requirement will be the [Telegram app](https://telegram.org/apps) (or third party alternatives) installed on your phone/laptop, or just a browser to run [Telegram Web](https://web.telegram.org/).


# ---> [Book your ticket for the next event](https://www.eventbrite.it/e/biglietti-deepgram2-creiamo-un-bot-per-il-deep-learning-77723576309) <---
## Wednesday 23rd of October 2019 @[Digital Tree - Innovation Hub](https://digitaltree.ai)

## Why is it important?

_Has it ever happened to you to have an idea that you would like to prototype faster?_ I know that is wrong to answer with a question to a question. But what we want is to allow you to implement some Machine Learning algorithm very fast, _on-the-fly_.

You won't need to go home, turn your computer on, open your IDE, copy some model from [Stackoverflow](https://stackoverflow.com/), becoming mad installing packages till you give up your awesome idea and go back to your 9-5 job.

You just need to open your Telegram app and click the right buttons. 

<p align="center">
  <img width="338" height="262" src="https://github.com/deepgramtech/deepgramtech.github.io/blob/master/img/telegrambot.png?raw=true">
</p>

Jokes apart, it will look something like:

<p align="center">
  <img width="542" height="590" src="https://github.com/deepgramtech/deepgramtech.github.io/blob/master/img/telegif.gif?raw=true">
</p>

while, somewhere else, something like the following is been creating:

<p align="center">
  <img width="650" height="250" src="https://github.com/deepgramtech/deepgramtech.github.io/blob/master/img/modelgif.gif?raw=true">
</p>

## This is a community based project.

No leaders here. Just passionate people desiring to learn, and _to create_. We organize **MeetUps** to make this real. If you are around Genova (Italy), come and build with us. 

### Where?
<a href="https://digitaltree.ai"><img align="right" width="100" height="100" src="https://github.com/deepgramtech/deepgramtech.github.io/blob/master/img/dtblack.png?raw=true">

At **[Digital Tree](https://digitaltree.ai), Viale Cembrano, 2, 16147 Genova GE.**



### When?

## [The next meetup will be on *Wednesday* 23rd of October 2019, from 18 to 20.](https://www.eventbrite.it/e/biglietti-deepgram2-creiamo-un-bot-per-il-deep-learning-77723576309)

 Every two weeks, usually on *Wednesday*, **from 18 to 20**. 
 
 To be up to date, sync with our [Google Calendar](https://calendar.google.com/calendar/embed?src=i8m9ckbo5l0o38bc98ocui6mp8%40group.calendar.google.com&ctz=Europe%2FRome
): [here](https://calendar.google.com/calendar/embed?src=i8m9ckbo5l0o38bc98ocui6mp8%40group.calendar.google.com&ctz=Europe%2FRome) you will find any upcoming event.

## Outline

The meetups are structured as little hackatons of two hours, where specific (but not really fixed) goals have to be achieved. 

Our goal is mostly _learning by doing_, so we won't focus strictly on the advancement of the project at each meeting, but on learning as much as we can. The following is a outline of the content of the next three meetups:

### [Meetup #0](https://github.com/deepgramtech/deepgramtech.github.io/blob/master/img/firstbotgif.gif?raw=true)

<a href="https://digitaltree.ai"><img align="center" width="438" height="510" src="https://github.com/deepgramtech/deepgramtech.github.io/blob/master/img/giffirstbot.gif?raw=true">

### [Meetup #1](https://www.eventbrite.it/e/biglietti-deepgram1-creiamo-un-bot-per-il-deep-learning-76718881239)

We will:

> - Make a Telegram bot to perform classification on MNIST dataset

> - Learn more about the [Python library Telepot](https://telepot.readthedocs.io/en/latest/)


### [Meetup #2](https://www.eventbrite.it/e/biglietti-deepgram2-creiamo-un-bot-per-il-deep-learning-77723576309)

We will:

> - Make the bot to use a pretrained simple Neural Network to Classify Dog and Cats

> - Make a [DeepDream](https://en.wikipedia.org/wiki/DeepDream) Telegram Version (input an image -> return a DeepDreamed image)



### Meetup #3

We will: 

> - Make a Telegram Channel with Datasets that can be used. First example: [MNIST](http://yann.lecun.com/exdb/mnist/).
 
> - Make the program to train a classifier on [MNIST](http://yann.lecun.com/exdb/mnist/).

> - Make the program to train an autoencoder on [MNIST](http://yann.lecun.com/exdb/mnist/).

## Questions?

Feel free to ask at antonio.marsella95@gmail.com or antoniomarsella@deepgram.tech, in the [Telegram Group](https://t.me/hackademiaitaly) or privately at [@marsantonio](https://t.me/marsantonio).
